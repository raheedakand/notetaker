import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

class Note{
  content: string;
  header: string;
  createdDate: Date;
  disabled: boolean = false;
}


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public noteList = new Array();
  constructor(public navCtrl: NavController) {

  }

  addNote(){
    let note = new Note();
    note.header = "First header";
    note.content = "This is the content of the nte";
    note.createdDate = new Date();
    this.noteList.push(note);
  }


  // deleteNote(note:Note){
  //   for(let i = 0; i  < this.noteList.length;i++){
  //       if(this.noteList[i] == note){
  //         delete this.noteList[i];
  //       }
  //   }
  // }
}
